
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Mhao Mhao',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Mhao Mhao Auction System' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Prompt:300,400,500,600,700|Material+Icons' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: 'rgba(255, 167, 38, 1)' },
  /*
  ** Global CSS
  */
  css: [
    {
      src: 'ant-design-vue/dist/antd.less',
      lang: 'less'
    },
    '~/css/style.less'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/base-container.js',
    '~/plugins/antd-ui',
    '~/plugins/directives.js',
    '~/plugins/filters.js',
    '~/plugins/vee-validate.js',
    '~/plugins/i18n.js',
    '~/plugins/font-awesome',
    { src: '~/plugins/vue-timers', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // ['semantic-ui-vue/nuxt', { css: true }]
    'vue-wait/nuxt'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },
    loaders: {
      less: {
        javascriptEnabled: true,
        modifyVars: {
          'primary-color': 'rgba(255, 167, 38, 1)'
        }
      }
    }
  }
}
